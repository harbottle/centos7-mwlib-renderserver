FROM centos:latest
MAINTAINER harbottle <grainger@gmail.com>
RUN yum -y install epel-release
RUN yum -y install https://harbottle.gitlab.io/ergel/7/x86_64/ergel-release-7-2.el7.x86_64.rpm
RUN yum -y install https://harbottle.gitlab.io/epypel/7/x86_64/epypel-release-7-1.el7.x86_64.rpm
RUN yum -y install createrepo
RUN yum -y install rubygem-colorize
RUN yum -y install rubygem-fpm
RUN yum -y install rubygem-java-properties
RUN yum -y install rubygem-POpen4
RUN yum -y install rubygem-rake
RUN yum -y install rubygem-rpm
