require "fpm"
require "yaml"
require "colorize"
require "popen4"
require "java-properties"

pad_length = 55
$stdout.sync = true
global_status = 0

# Function to convert (yaml) object to hash with symbol keys
def symbolize(obj)
    return obj.inject({}){|memo,(k,v)| memo[k.to_sym] =  symbolize(v); memo} if obj.is_a? Hash
    return obj.inject([]){|memo,v    | memo           << symbolize(v); memo} if obj.is_a? Array
    return obj
end

print "Loading 'rpm.yml'".ljust(pad_length, padstr='.')
begin
  rpm_packages = symbolize(YAML::load_file(File.join(Dir.pwd, 'rpm.yml')))[:rpm_packages]
rescue StandardError => error 
  print "Failure:\n#{error.to_s.strip}\n".red
  exit 2
else
  print "Success.\n".green
end 

rpm_packages.each do |package, options|
  dir_package = FPM::Package::Dir.new
  ['name', 'description', 'maintainer', 'vendor', 'license', 'url', 'iteration',
   'config_files', 'dependencies', 'provides', 'conflicts',
   'replaces'].each do |setting|
    unless options[setting.to_sym].nil?
      dir_package.send("#{setting}=", options[setting.to_sym])
    end
  end
  if options[:version].respond_to?(:keys)
    property = options[:version][:property]
    properties = JavaProperties.load(options[:version][:file])
    dir_package.version = properties[property.intern]
  else
    dir_package.version = options[:version]
  end
  dir_package.scripts[:before_install] = options[:before_install]
  dir_package.scripts[:after_install] = options[:after_install]
  options[:file_sources].each do |src, src_options|
    dir_package.attributes[:prefix] = src_options[:prefix]
    dir_package.attributes[:chdir] = src_options[:chdir]
    dir_package.input(src_options[:path])
  end
  print "RPM build ('#{package}')".ljust(pad_length, padstr='.')
  output = 'NAME-VERSION-ITERATION.ARCH.rpm'
  begin
    rpm = dir_package.convert(FPM::Package::RPM)
    rpm.output(rpm.to_s(output))
  rescue StandardError => error 
    print "Failure:\n#{error.to_s.strip}\n".red
    global_status += 1
  else
    print "Success.\n".green
  end
end

exit global_status
