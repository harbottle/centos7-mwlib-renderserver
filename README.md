# centos7-mediawiki123-Collection
[![build status](https://gitlab.com/harbottle/centos7-mwlib-renderserver/badges/master/build.svg)](https://gitlab.com/harbottle/centos7-mwlib-renderserver/builds)

Build MediaWiki mwlib renderserver RPM for CentOS 7 using GitLab CI.

[Browse and download](https://gitlab.com/harbottle/centos7-mwlib-renderserver/builds/artifacts/master/browse?job=build)
the generated RPM packages from the latest successful CI build.

Generated RPM package:
 - `mwlib-renderserver`

Dependencies:
 - `python-mwlib`
 - `python-mwlib.rl`
 
Dependency RPMs can be found in:
[epypel](https://harbottle.gitlab.io/epypel/7/x86_64/).

Config files:

Managing services:
